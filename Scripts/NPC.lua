-- NPC example script
Script.direction = 1

function Script:Start()
	self.footstepsManager = FootstepsManager:Create(self.entity)
end


function Script:UpdateWorld()
	--update footsteps and play sound if needed
	self.footstepsManager:Update()

	--toggle movement
	if window:KeyHit(Key.Space) then
		self.enabled = not self.enabled
	end

	--npc movement
	if self.enabled then
		self.entity:SetInput(0,self.direction*3,0)
	else
		self.entity:SetInput(0,0,0)
	end

	if math.abs(self.entity.position.z)>4 and (self.entity.position.z<0) == (self.direction<0) then 
		self.direction = self.direction*(-1)	--switch direction every 4 metes
	end
end


function Script:Detach()	--we need to delete footsteps audio source when object is deleted
	self.footstepsManager:Detach()
end