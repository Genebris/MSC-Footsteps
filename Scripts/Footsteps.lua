FootstepsManager = {}	--creating a "class"
FootstepsManager.delayTime = 0.6	--delay between footsteps in seconds
FootstepsManager.nextTime = 0	--time when next footstep should be triggered


function FootstepsManager:Create(entity, isPlayer)	--creates and returns footsteps manager object for the particular entity
	if entity==nil then Debug:Error("Entity cannot be nil.") end

	local footstepsManager = {}		--creating manager object
	footstepsManager.entity = entity	--entity that manager was created for
	
	if not isPlayer then	--use 3D sound if this is not a player character
		footstepsManager.source = Source:Create()	--audio source
		footstepsManager.source:SetLoopMode(false)
		footstepsManager.source:SetRange(20)
		footstepsManager.source:Stop()
	end

	local k,v	--this part is needed to copy all variables and functions of manager class to manager object
	for k,v in pairs(FootstepsManager) do
		footstepsManager[k] = v
	end

	return footstepsManager	--returning manager object
end



function FootstepsManager:Update()
	if self.nextTime>Time:GetCurrent() then return end	--time has not come
	--inside this function "self" is footsteps manager object that calls this function

	local pickinfo = PickInfo()
	local p1 = self.entity:GetPosition(true)+Vec3(0,0.1,0)
	local p2 = p1 - Vec3(0,2,0)
	local name
	if (world:Pick(p1,p2,pickinfo,0.2,true,Collision.LineOfSight)) then	--get object under character
		local material = tolua.cast(pickinfo.entity, "Model"):GetSurface(0):GetMaterial()
		if material then	-- there can be a surface without material
			name = FileSystem:StripAll(material:GetPath()):lower()	--name of material in lower case
		end
	end
	self.materialName = name	--debug info, can be deleted

	if self.entity:GetVelocity():Length() > 0.1 and not self.entity:GetAirborne() then	--entity is moving and not in midair
		local sound = self:GetSound(name)
		if self.source then	--NPCs have audio source, play sound from it
			self.source:SetSound(sound)
			self.source:SetPosition(self.entity:GetPosition(true))
			self.source:Play()
		else	--player has no source, no 3D sound
			sound:Play()
		end
	end
	
	self.nextTime = Time:GetCurrent()+self.delayTime*1000
end



function FootstepsManager:GetSound(name)
	if not name then name = "concrete" end	--consider concrete material if no name was set

	for key,value in pairs(footstepsSounds) do	--loop through available sound types and search name inside material name
		if string.find(name,key) then	--if table name is found in material name
			self.soundType = key	--debug info, can be deleted
			return value[math.random(1,#value)]	--"value" is table of sounds of needed material type, pick random sound from this table
		end
	end

	self.soundType = "concrete"	--debug info, can be deleted
	return footstepsSounds["concrete"][math.random(1,#footstepsSounds["concrete"])]	--if needed sound type wasn't found then concrete sound is used
end



function FootstepsManager:Detach()	--cal this when character is deleted to remove audio source
	if self.source then
		self.source:Stop()
		self.source:Release()
	end
end









function LoadFootstepsSounds()	--this function is global and isn't related to footsteps manager class
	footstepsSounds = {}	--global table for sounds
	LoadFootstepsFolder("concrete", 4)	--load 4 sound files from concrete folder
	LoadFootstepsFolder("wood", 8)	--this name will be used to determine material type
	LoadFootstepsFolder("dirt", 6)	--all materials with "dirt" in their name will use these sounds
	LoadFootstepsFolder("snow", 3)
	LoadFootstepsFolder("grass", 5)
end

function LoadFootstepsFolder(name, amount)	--loads sounds from a folder
	footstepsSounds[name]={}
	for i=1,amount do
		footstepsSounds[name][i] = Sound:Load("sound/footsteps/"..name.."/"..i..".wav")
	end
end















--