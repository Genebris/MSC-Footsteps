-- player example script
import "scripts/footsteps.lua"

function Script:Start()
	LoadFootstepsSounds()	--load all sound files, this can be done in main script
	self.footstepsManager = FootstepsManager:Create(self.entity, true)	--create and save footsteps manager object. true means this is a player character
	Listener:Create(self.entity)	--to hear 3D sounds
end

function Script:UpdateWorld()
	--player movement
	local move = ((window:KeyDown(Key.Up) and 1 or 0) - (window:KeyDown(Key.Down) and 1 or 0))*2.5
	local strafe = ((window:KeyDown(Key.Right)and 1 or 0) - (window:KeyDown(Key.Left) and 1 or 0))*2.5
	if window:KeyDown(Key.Shift) then	--running
		move=move*2
		strafe=strafe*2
		self.footstepsManager.delayTime=0.3	--modifying step interval when running
	else
		self.footstepsManager.delayTime=0.6	--walking step interval
	end
	self.entity:SetInput(0,move,strafe,0,false,9999,9999)

	--update footsteps and play sound if needed
	self.footstepsManager:Update()
end







--only for debug info
function Script:PostRender(context)
	context:SetBlendMode(Blend.Alpha)
	context:DrawText("Arrows to move player",30,100)
	context:DrawText("Shift to run",30,115)
	context:DrawText("Space to toggle NPC",30,140)
	if self.footstepsManager.materialName then context:DrawText("Material name: "..self.footstepsManager.materialName,30,220) end
	if self.footstepsManager.soundType then context:DrawText("Selected sound type: "..self.footstepsManager.soundType,30,240) end
end